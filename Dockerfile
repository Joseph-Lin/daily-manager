FROM alpine:latest

RUN apk add --no-cache python3 \
    && apk add --no-cache py3-pip

WORKDIR /app

COPY . /app

RUN pip3 --no-cache-dir install -r requirements.txt

RUN chmod +x /app/run.sh

CMD ["/app/run.sh"]


