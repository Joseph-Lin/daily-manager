# Daily Manager
An open source Task Management system powered by Flask and SQLAlchemy, with user authentication and login system implemented
 

**Created by Joseph Lin**
  

# Installation

Install all the dependencies

```
pip3 install -r requirements.txt 
```

Create the db table

```
python manage.py create_db
```

# Run the app

Run the app

```
python run.py
```