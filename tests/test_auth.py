import unittest

from config import AuthTestConfig
from flask import url_for
from flask_login import current_user
from app import create_app
from app.auth import User
from app.database import db


class AuthTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app(AuthTestConfig)
        self.app_context = self.app.test_request_context()
        self.app_context.push()
        db.app = self.app
        db.create_all()
        self.client = self.app.test_client(use_cookies=True)

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_signup_route(self):
        rv = self.client.get(url_for('auth.signup'))
        self.assertEqual(rv.status_code, 200)
    
    def test_login_route(self):
        rv = self.client.get(url_for('auth.login'))
        self.assertEqual(rv.status_code, 200)

    def test_add_user_with_password_hashing(self):
        user = User.create(email='test@gmail.com', password='mysecret', name="joseph")
        self.assertEqual(user.email, 'test@gmail.com')
        self.assertNotEqual(user.password, 'mysecret', 'Password not hashed')
        self.assertTrue(user.is_correct_password('mysecret'))
        self.assertEqual(user.name, 'joseph')

    def test_valid_login_submit(self):
        with self.client:
            user = User.create(email='right@gmail.com', password='mysecret', name='joseph')
            form_data = {
                'email': 'right@gmail.com',
                'password': 'mysecret'
            }
            rv = self.client.post(url_for('auth.login_post'), data=form_data, follow_redirects=True)
            self.assertEqual(rv.status_code, 200)
            self.assertEqual(current_user.id, user.id)
            rv = self.client.get('/view')
            self.assertEqual(rv.status_code, 200)

    def test_incorrect_password_display_message(self):
        user2 = User.create(email='wrong@gmail.com', password='mysecret', name="joseph")
        form_data = {
            'email': 'wrong@gmail.com',
            'password': 'wrongpassword'
        }
        rv = self.client.post(url_for('auth.login_post'), data=form_data, follow_redirects=True)
        self.assertEqual(rv.status_code, 200)
        rv = self.client.get('/view')
        self.assertEqual(rv.status_code, 302)
        

    # Trying to access login_required pages should redirect back to login pages
    def test_login_required(self):
        rv = self.client.get('/view')
        self.assertEqual(rv.status_code, 302)