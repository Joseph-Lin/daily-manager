import unittest

from config import TestConfig
from flask import url_for
from flask_login import current_user, login_user
from app import create_app
from app.core import Task
from app.auth import User
from app.database import db

class CoreTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app(TestConfig)
        self.app_context = self.app.test_request_context()
        self.app_context.push()
        db.app = self.app
        db.create_all()
        self.client = self.app.test_client(use_cookies=True)

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_dashboard_route(self):
        with self.client:
            user = User.create(email='test@gmail.com', password='mysecret', name="joseph")
            form_data = {
                'email': 'test@gmail.com',
                'password': 'mysecret'
            }
            rv = self.client.post(url_for('auth.login_post'), data=form_data, follow_redirects=True)
            rv = self.client.get('/view')
            self.assertEqual(rv.status_code, 200)

            # test add single task
            task_data = {
                'task-name':'test task 1',
            }
            rv = self.client.post(url_for('core.view'), data=task_data, follow_redirects=True)
            self.assertEqual(rv.status_code, 200)

            # test add multiple task 
            Task.create('test task 2', current_user.id)
            Task.create('test task 3', current_user.id)
            t = Task.query.filter_by(created_by=current_user.id).all()
            self.assertEqual(t[0].task_name, "test task 1")
            self.assertEqual(t[1].task_name, "test task 2")
            self.assertEqual(t[2].task_name, "test task 3")

            # test delete single task
            rv = self.client.get('/view/delete/1')
            self.assertEqual(rv.status_code, 302)
            t = Task.query.filter_by(created_by=current_user.id).all()
            self.assertEqual(t[0].task_name, "test task 2")

            # test delete multiple task
            rv = self.client.get('/view/delete/2')
            rv = self.client.get('/view/delete/3')
            t = Task.query.filter_by(created_by=current_user.id).all()
            self.assertEqual(len(t), 0)
