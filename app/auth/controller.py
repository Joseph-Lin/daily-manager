from flask import render_template, redirect, url_for, request, flash
from flask_login import login_user, logout_user, login_required
# encrytion and decrytion
from werkzeug.security import generate_password_hash, check_password_hash

from . import auth 
from app.auth.models import User
from app.database import db
from app.core import core

@auth.route('/login')
def login():
    return render_template('auth/login.html')

@auth.route('/signup')
def signup():
    return render_template("auth/signup.html")

@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('core.home'))

# handle login authorization
@auth.route('/login', methods=['POST'])
def login_post():
    email = request.form.get('email')
    password = request.form.get('password')

    # check if the user exist
    user = User.query.filter_by(email=email).first()

    # if user do not exist or password incorrect
    if not user or not check_password_hash(user.password, password):
        flash('Please check your login details and try again.')
        return redirect(url_for('auth.login'))

    login_user(user)
    return redirect(url_for('core.view'))


# handle sign up authorization
@auth.route('/signup', methods=['POST'])
def signup_post():
    email = request.form.get('email')
    name = request.form.get('name')
    password = request.form.get('password')

    #if a user is returned, that means the email is already registere
    user = User.query.filter_by(email=email).first() 

    if user: 
        flash('Email address already exists')
        # if a user is found, we want to redirect back to signup page so user can try again
        return redirect(url_for('auth.signup'))

    if not email:
        flash('Email address cannot be empty')
        return redirect(url_for('auth.signup'))
    
    if not name:
        flash('Name address cannot be empty')
        return redirect(url_for('auth.signup'))

    if not password:
        flash('Password address cannot be empty')
        return redirect(url_for('auth.signup'))

    # create a new user with the form data. Hash the password so the plaintext version isn't saved.
    new_user = User(email=email, name=name, password=generate_password_hash(password, method='sha256'))

    # add the new user to the database
    db.session.add(new_user)
    db.session.commit()
    return redirect(url_for('auth.login'))