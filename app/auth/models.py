from flask_login import UserMixin
from app.database import db
from app.core import Base

from werkzeug.security import generate_password_hash, check_password_hash

class User(UserMixin, Base):
    __tablename__ = 'user'

    #id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True, nullable=False)
    password = db.Column(db.String(100), nullable=False)
    name = db.Column(db.String(1000), nullable=False)

    @staticmethod
    def create(email, name, password):
        u = User(email=email, name=name, password=generate_password_hash(password, method='sha256'))
        db.session.add(u)
        try:
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
        return u

    def is_correct_password(self, plaintext):
       return check_password_hash(self.password, plaintext)