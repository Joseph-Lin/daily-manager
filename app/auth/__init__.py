from flask import Blueprint

# __name__ refers to the name of this file
auth = Blueprint('auth', __name__, static_folder='static', template_folder='templates/auth')

from .models import User
from . import controller