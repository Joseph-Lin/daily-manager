from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

from app.auth import User