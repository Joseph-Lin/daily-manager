from flask import render_template, url_for, redirect, request, flash
from flask_login import login_required, current_user


from . import core
from app.core.models import Task
from app.database import db


@core.route('/')
def home():
    return render_template('core/home.html')

# render tasks and create new task if it's post method
@core.route("/view", methods=['POST','GET'])
@login_required
def view():

    if request.method == 'POST':
        task_name = request.form.get('task-name')
        created_by = current_user.id
        completed = False

        if task_name == "":
            flash('Please enter your new task')
            return redirect(url_for('core.view'))
        # create new task with form data
        new_task = Task(created_by=created_by, task_name=task_name, completed=completed)

        # add new task to database, user id as foreign key
        db.session.add(new_task)
        db.session.commit()
        return redirect(url_for('core.view'))
    else:
        all_tasks = Task.query.filter_by(created_by=current_user.id).all()
        return render_template('core/view.html', name=current_user.name, tasks=all_tasks)


@core.route("/view/delete/<int:id>")
@login_required
def delete(id):
    task = Task.query.get_or_404(id)
    db.session.delete(task)
    db.session.commit()
    return redirect('/view')

@core.route("/view/complete/<int:id>")
@login_required
def complete(id):
    task = Task.query.get_or_404(id)
    print(task.completed)
    if task.completed:
        task.completed = False
    else:
        task.completed = True
    db.session.commit()
    return redirect('/view')


    







    


