from app.database import db

class Base(db.Model):
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)


class Task(Base):

    __tablename__ = 'task'

    created_by = db.Column(db.Integer, db.ForeignKey('user.id'))
    task_name = db.Column(db.String(100),nullable=False)
    completed = db.Column(db.Boolean, default=True)
    
    @staticmethod
    def create(task_name, created_by):
        t = Task(task_name=task_name, created_by=created_by, completed=False)
        db.session.add(t)
        try:
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
        return t