from flask import Blueprint

from .models import Base, Task

# __name__ refers to the name of this file
core = Blueprint('core', __name__, static_folder='static', template_folder='templates/core')

from . import controller