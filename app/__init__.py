from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

from config import BaseConfig

from app.database import db

from app.auth import auth as auth_blueprint
from app.core import core as core_blueprint
from app.auth.models import User


def create_app(config=BaseConfig):
    app = Flask(__name__)
    app.config.from_object(config)

    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    db.init_app(app)

    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))
    
    #register all blueprints
    register_blueprints(app)

    return app


def register_blueprints(app):
    app.register_blueprint(auth_blueprint)
    app.register_blueprint(core_blueprint)

    